'''
Created on 4 nov. 2020

@author: RSSpe
'''

class ListaDoblementeEnlazada:

    class Nodo:

        def __init__(self, dato=None):
            self.__dato = dato
            self.__siguiente = None
            self.__anterior = None

        @property
        def dato(self):
            return self.__dato

        @property
        def siguiente(self):
            return self.__siguiente

        @siguiente.setter
        def siguiente(self, siguiente):
            self.__siguiente = siguiente

        @property
        def anterior(self):
            return self.__anterior

        @anterior.setter
        def anterior(self, anterior):
            self.__anterior = anterior
        
        def __str__(self):
            return f"Nodo=[Dato {self.__dato}]"


    def __init__(self):
        self.__primero = self.Nodo()
        self.__ultimo = self.Nodo()
        self.__primero.siguiente = self.__ultimo
        self.__ultimo.anterior = self.__primero

    def __verificarListaVacia(self):
        return self.__primero.siguiente==self.__ultimo
    
    def mostrarUltimoElemento(self):
        if self.__verificarListaVacia():
            print("No hay elementos")
        else:
            tmp = self.__primero.siguiente

            while tmp.siguiente!=self.__ultimo:
                tmp = tmp.siguiente

            print(tmp)

    def mostrarPrimerElemento(self):
        if self.__verificarListaVacia():
            print("No hay elementos")
        else:
            print(self.__primero.siguiente)
    
    def buscarElemento(self, dato):
        if self.__verificarListaVacia():
            print("No hay elementos")
        else:
            tmp = self.__primero
            encontrado = False

            while(tmp!=self.__ultimo):
                tmp = tmp.siguiente
                if tmp.dato==dato:
                    encontrado = True
                    break
            
            if encontrado:
                print("Encontrado")
            else:
                print("No se ha encontrado")

    def vaciarLista(self):
        self.__primero.siguiente = self.__ultimo
        self.__ultimo.anterior = self.__primero
    
    def agregarInicio(self, dato):
        self.agregarEntre(self.__primero, self.Nodo(dato), self.__primero.siguiente)
        return True
    
    def agregarFin(self, dato):
        self.agregarEntre(self.__ultimo.anterior, self.Nodo(dato), self.__ultimo)
        return True
    
    def agregarEntre(self, primero, nuevo, ultimo):
        primero.siguiente = nuevo
        nuevo.anterior = primero
        nuevo.siguiente = ultimo
        ultimo.anterior = nuevo
    
    def cantidadElementos(self):
        tmp = self.__primero.siguiente
        cont = 0

        while tmp!=self.__ultimo:
            tmp = tmp.siguiente
            cont+=1
        
        return cont

    def verLista(self):
        if self.__verificarListaVacia():
            print("No hay elementos")
        else:
            tmp = self.__primero.siguiente

            while(tmp!=self.__ultimo):
                print(f"<-{tmp}->", end="")
                tmp=tmp.siguiente
    
    def eliminarInicio(self):
        if self.__primero.siguiente==self.__ultimo:
            print("Sin elementos")
            return None
        else:
            eliminado = self.__primero.siguiente
            
            self.__primero.siguiente = self.__primero.siguiente.siguiente
            self.__primero.siguiente.anterior = self.__primero
            return eliminado

    def eliminarFin(self):
        if self.__primero.siguiente==self.__ultimo:
            print("Sin elementos")
            return None
        else:
            eliminado = self.__ultimo.anterior
            
            self.__ultimo.anterior = self.__ultimo.anterior.anterior
            self.__ultimo.anterior.siguiente = self.__ultimo
            return eliminado

    def eliminarElementoEspecifico(self, dato):
        if self.__verificarListaVacia():
            return None
        else:
            eliminado = None

            if dato==self.__primero.siguiente.dato:
                eliminado = self.__primero.siguiente
                self.__primero.siguiente = eliminado.siguiente
                eliminado.siguiente.anterior = self.__primero
                return eliminado
            elif dato==self.__ultimo.anterior.dato:
                eliminado = self.__ultimo.anterior
                self.__ultimo.anterior = eliminado.anterior
                eliminado.anterior.siguiente = self.__ultimo
                return eliminado
            else:
                anterior = None
                siguiente = self.__primero.siguiente
                
                while siguiente!=self.__ultimo:
                    if dato==siguiente.dato:
                        eliminado = siguiente
                        eliminado.siguiente.anterior = anterior
                        anterior.siguiente = siguiente.siguiente
                        return eliminado
                        break
                    anterior = siguiente
                    siguiente = siguiente.siguiente

            print("\nNo existe ese elemento")
            return eliminado
